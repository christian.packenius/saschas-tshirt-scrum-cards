package com.packenius.tshirtcards4sasa;

/**
 * Integer-Intervall.
 */
public class IntRange {
  /**
   * Von (inklusive)...
   */
  public final int value1;

  /**
   * ...bis (exklusive).
   */
  public final int value2;

  public IntRange(int value1, int value2) {
    this.value1 = value1;
    this.value2 = value2;
  }

  public int getSize() {
    return value2 - value1;
  }
}
