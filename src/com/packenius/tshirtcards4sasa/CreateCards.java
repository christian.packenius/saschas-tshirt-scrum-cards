package com.packenius.tshirtcards4sasa;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CreateCards {
  /**
   * Pfad zur Bilddatei, auf der Beispiel-Shirts zu finden sind.
   */
//  private static String shirtsPath = "docs/t-shirts.png";
  private static String shirtsPath = "docs/t-shirt-1271443.png";

  private static int count = 0;

  public static void main(String[] args) throws IOException {
    // In diesem Bild sind 4 T-Shirts nebeneinander drin.
    BufferedImage fourImageShort = ImageIO.read(new File(shirtsPath));

    // Die vier Shirts voneinander trennen.
    List<BufferedImage> shirts = getShirts(fourImageShort);

    // Eine einzelne Karte erstellen.
    // Index 1 für Männer, Index 3 für Frauen.
    createCard(shirts.get(1), new File("docs/images/violin-2412357.jpg"), "Verdana", 0xff424242, 0xff575757);
    createCard(shirts.get(3), new File("docs/images/violin-2412357.jpg"), "Verdana", 0xff424242, 0xff575757);

    createCard(shirts.get(1), new File("docs/images/composing-2391033_1920.jpg"), "Palatino Linotype", 0xff8B4513, 0xffCD853F);
    createCard(shirts.get(3), new File("docs/images/dog-647528_1920.jpg"), "Segoe Print", 0xff008000, 0xff404040);
    createCard(shirts.get(1), new File("docs/images/dusk-404072_1920.jpg"), "Calibri", 0xff8B4513, 0xffCD853F);
    createCard(shirts.get(3), new File("docs/images/man-815795_1920.jpg"), "Segoe Print", 0xff602020, 0xff202060);
    createCard(shirts.get(1), new File("docs/images/psychedelic-1084082_1920.jpg"), "Arial", 0xff008080, 0xff4B0082);
    createCard(shirts.get(3), new File("docs/images/rose-729509_1920.jpg"), "Cambria", 0xffb04040, 0xffbA8500);
    createCard(shirts.get(1), new File("docs/images/space-911785_1920.jpg"), "System Fett", 0xff4a8d32, 0xff82392E);
    createCard(shirts.get(3), new File("docs/images/texture-1909992_1920.jpg"), "Lucida Console Standard", 0xff800000, 0xff0000cd);
    createCard(shirts.get(3), new File("docs/images/witchs-house-1635770_1920.jpg"), "Verdana", 0xff602020, 0xff206020);
    createCard(shirts.get(1), new File("docs/images/young-3161821_1920.jpg"), "Rubik", 0xffff0000, 0xff0000ff);
  }

  /**
   * Für die Daten eines jedes Shirts eigene Struktur erstellen.
   */
  private static List<BufferedImage> getShirts(BufferedImage fourImageShort) {
    // Bild an Hand der komplett unsichtbaren Zeilen und Spalten in Zellen einteilen.
    int width = fourImageShort.getWidth();
    int height = fourImageShort.getHeight();

    // "true" bedeutet: Hier sind nicht-unsichtbare Pixel (also T-Shirt-Pixel).
    boolean[] cols = new boolean[width];
    boolean[] rows = new boolean[height];
    fillTruePixelArrays(fourImageShort, width, height, cols, rows);

    // Aufteilung der T-Shirt-Spalten.
    List<IntRange> colRanges = getRanges(cols);
    List<IntRange> rowRanges = getRanges(rows);
    System.out.println(colRanges.size() + " x " + rowRanges.size() + " T-Shirts found.");

    List<BufferedImage> shirts = new ArrayList<>();
    for (IntRange colRange : colRanges) {
      for (IntRange rowRange : rowRanges) {
        shirts.add(fourImageShort.getSubimage(colRange.value1, rowRange.value1, colRange.getSize(), rowRange.getSize()));
      }
    }

    return shirts;
  }

  /**
   * Füllt spalten- und zeilenweise, wo sichtbare Pixel sind (dort sind die
   * Shirts zu finden).
   */
  private static void fillTruePixelArrays(BufferedImage fourImageShort, int width, int height, boolean[] cols, boolean[] rows) {
    for (int x = width - 1; x >= 0; x--) {
      for (int y = height - 1; y >= 0; y--) {
        boolean visiblePixel = (fourImageShort.getRGB(x, y) & 0xff000000) != 0;
        cols[x] |= visiblePixel;
        rows[y] |= visiblePixel;
      }
    }
  }

  /**
   * Findet die Bereichs im übergebenen boolean-Array, die komplett true sind.
   * (Hintergrund: Dort sind die T-Shirts im ansonsten durchsichtigen Bild.)
   */
  private static List<IntRange> getRanges(boolean[] bools) {
    List<IntRange> ranges = new ArrayList<>();
    int bLength = bools.length;
    boolean last = false;
    int value1 = 0;
    for (int i = 0; i < bLength; i++) {
      if (!last && bools[i]) {
        value1 = i;
      } else if (last && !bools[i]) {
        ranges.add(new IntRange(value1, i));
      }
      last = bools[i];
    }
    if (last) {
      ranges.add(new IntRange(value1, bLength));
    }

    return ranges;
  }

  /**
   * Erstellt mit dem vorgegebenen T-Shirt und dem Bild, das darauf abgebildet
   * werden soll eine Karte.
   */
  private static void createCard(BufferedImage shirtBorder, File backgroundFile, String fontName, int textColor1, int textColor2) throws IOException {
    // Aus beiden Bildern ein farbiges Shirt erstellen.
    BufferedImage shirt = getColorizedShirt(shirtBorder, backgroundFile);

    // Das Zielbild groß genug für den Drucker wählen, Verhältnis 8:5 für Visitenkarten.
    BufferedImage card = new BufferedImage(5000, 8000, BufferedImage.TYPE_INT_ARGB);
    Graphics2D g2d = (Graphics2D) card.getGraphics();
    g2d.setColor(Color.WHITE);
    g2d.fillRect(0, 0, card.getWidth(), card.getHeight());
    g2d.dispose();

    // Hintergrund und Shirt auf die Karte zeichnen.
    drawShirtOnCard(card, shirt, backgroundFile);
    BufferedImage cardWithoutText = card;

    new File("result").mkdirs();

    for (String text : new String[]{"XXL", "XL", "L", "M", "S", "XS", "XXS"}) {
      System.out.print(" " + text);
      card = cloneCardImage(cardWithoutText);

      // Text oben (links und rechts).
      drawTextOnCard(card, text, textColor1, textColor2, fontName);

      // Karte drehen.
      card = flipCard(card);

      // Text unten (links und rechts).
      drawTextOnCard(card, text, textColor1, textColor2, fontName);

      // Karte drehen (nicht mehr nötig, ist aber schöner auf der Festplatte).
      card = flipCard(card);

      // Karte abspeichern.
      File shirtFile = new File("result/" + count + "-" + text + ".png");
      ImageIO.write(card, "PNG", shirtFile);

      // Nach jeder Bilderstellung selbiges per Standardprogramm öffnen?
      // Desktop.getDesktop().open(shirtFile);
    }
    System.out.println();

    count++;
  }

  private static BufferedImage cloneCardImage(BufferedImage card) {
    BufferedImage clone = new BufferedImage(card.getWidth(), card.getHeight(), BufferedImage.TYPE_INT_ARGB);
    Graphics2D g2d = (Graphics2D) clone.getGraphics();
    g2d.drawImage(card, 0, 0, null);
    g2d.dispose();
    return clone;
  }

  /**
   * Dreht die Karte um 180°, damit die Schrift auf die andere Seite gezeichnet werden kann.
   */
  private static BufferedImage flipCard(BufferedImage card) {
    int width = card.getWidth();
    int height = card.getHeight();
    BufferedImage c2 = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    for (int y = height - 1, y2 = 0; y >= 0; y--, y2++) {
      for (int x = width - 1, x2 = 0; x >= 0; x--, x2++) {
        c2.setRGB(x2, y2, card.getRGB(x, y));
      }
    }
    return c2;
  }

  private static BufferedImage getColorizedShirt(BufferedImage shirtBorder, File backgroundFile) throws IOException {
    // Erst einmal wird das Hintergrundbild auf die gleiche Größe gebracht wie
    // das T-Shirt.
    int width = shirtBorder.getWidth();
    int height = shirtBorder.getHeight();
    System.out.println("Shirt-Größe: " + width + "x" + height);
    BufferedImage background = resizeBackground(width, height, backgroundFile);

    // Nun wird ein neues Bild mit gleicher Größe erstellt.
    BufferedImage colorizedShirt = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

    // Füllen des neuen Bildes (Pixel für Pixel).
    for (int y = height - 1; y >= 0; y--) {
      for (int x = width - 1; x >= 0; x--) {
        colorizedShirt.setRGB(x, y, getResultShirtColor(x, y, background, shirtBorder));
      }
    }
    return colorizedShirt;
  }

  /**
   * Hintergrund in die angegebene Größe umrechnen (erst verkleinern oder
   * vergrößern, dann an den Seiten minimal wegnehmen).
   */
  private static BufferedImage resizeBackground(int width, int height, File backgroundFile) throws IOException {
    return resizeBackground(width, height, ImageIO.read(backgroundFile));
  }

  private static BufferedImage resizeBackground(int width, int height, BufferedImage image) {
    // Erstmal schauen, welcher Faktor zu nehmen ist.
    // Es muss so gewählt werden, dass mindestens eine Seite passend ist und
    // die andere eventuell zu groß ist (außer dass zufällig die Proportionen
    // identisch sind).
    int oldWidth = image.getWidth();
    double xFactor = oldWidth / (double) width;
    int oldHeight = image.getHeight();
    double yFactor = oldHeight / (double) height;
    double factor = Math.min(xFactor, yFactor);

    // Den Faktor so wählen, dass eine Seite passend ist.
    int newWidth = (int) (oldWidth / factor);
    int newHeight = (int) (oldHeight / factor);
    Image image2 = image.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);

    // Jetzt muss entweder oben und unten oder aber links und rechts etwas
    // abgeschnitten werden. Auf jeden Fall wollen wir den Mittelteil haben.
    int dx = (newWidth - width) / 2;
    int dy = (newHeight - height) / 2;

    BufferedImage subimage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    Graphics2D g2d = (Graphics2D) subimage.getGraphics();
    g2d.drawImage(image2, -dx, -dy, null);
    g2d.dispose();

    return subimage;
  }

  /**
   * Ermittelt für den angegebenen Punkt die Zielfarbe.
   */
  private static int getResultShirtColor(int x, int y, BufferedImage background, BufferedImage shirtBorder) {
    int rgb = shirtBorder.getRGB(x, y);

    // Unsichtbar bleibt unsichtbar.
    if ((rgb & 0xff000000) == 0) {
      return 0;
    }

    // Hell wird zu Hintergrund.
    if ((rgb & 0x0000ff00) > 0x0000dd00) {
      return background.getRGB(x, y);
    }

    // Alles andere wird zu schwarz - hoffentlich genug Randfarbe dafür.
    return 0xff000000;
  }

  /**
   * Zeichnet das Shirt vielfach auf die Karte, so dass kein freier Raum mehr
   * übrig bleibt. Die Breite des Shirts wird als Maßangabe verwendet.
   */
  private static void drawShirtOnCard(BufferedImage card, BufferedImage shirt, File backgroundFile) throws IOException {
    double factor = card.getWidth() / (double) shirt.getWidth();

    // Den Hintergrund etwas heller als Hintergrund zeichnen.
    BufferedImage background = resizeBackground(card.getWidth(), card.getHeight(), backgroundFile);
    BufferedImage lightBackground = enlightImage(background);
    Graphics2D g2d = (Graphics2D) card.getGraphics();
    g2d.drawImage(lightBackground, 0, 0, null);
    g2d.dispose();

    // Das T-Shirt fast maximal groß, zum Schluss.
    drawShirtOnCard(card, shirt, factor * 0.95, 0.50, 0.50); // Fett in der Mitte, etwas hoch
  }

  private static void drawTextOnCard(BufferedImage card, String text, int textColor1, int textColor2, String fontName) {
    Graphics2D g2d;
    g2d = (Graphics2D) card.getGraphics();
    g2d.setFont(new Font(fontName, Font.BOLD, card.getWidth() / 9));
    int textWidth = g2d.getFontMetrics().stringWidth(text);
    int textHeight = g2d.getFontMetrics().getHeight();
    g2d.setColor(new Color(textColor1));
    g2d.drawString(text, textHeight / 2, textHeight); // Links oben.
    g2d.setColor(new Color(textColor2));
    g2d.drawString(text, card.getWidth() - textHeight / 2 - textWidth, textHeight); // Rechts oben.
    g2d.dispose();
  }

  /**
   * Das Image kopieren und "heller" machen, für den Hintergrund.
   */
  private static BufferedImage enlightImage(BufferedImage shirt) {
    int width = shirt.getWidth();
    int height = shirt.getHeight();
    BufferedImage light = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

    for (int y = height - 1; y >= 0; y--) {
      for (int x = width - 1; x >= 0; x--) {
        int rgb = shirt.getRGB(x, y);
        if ((rgb & 0xff000000) == 0xff000000) {
          int r = (255 + ((rgb >> 16) & 255)) / 2;
          int g = (255 + ((rgb >> 8) & 255)) / 2;
          int b = (255 + ((rgb >> 0) & 255)) / 2;
          light.setRGB(x, y, 0xff000000 | (r << 16) | (g << 8) | b);
        }
      }
    }

    return light;
  }

  private static void drawShirtOnCard(BufferedImage card, BufferedImage shirt, double factor, double mx, double my) {
    int width = (int) (factor * shirt.getWidth());
    int height = (int) (factor * shirt.getHeight());

    int miX = (int) (card.getWidth() * mx);
    int miY = (int) (card.getHeight() * my);

    Graphics2D g2d = (Graphics2D) card.getGraphics();
    g2d.drawImage(shirt, miX - width / 2, miY - height / 2, width, height, null);
    g2d.dispose();
  }
}
